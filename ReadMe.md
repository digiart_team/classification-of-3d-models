## DigiArt (http://digiart-project.eu/)
`` Classification of 3D Models using Convolutional Neural Networks in Keras ``

This repository contains code for DigiArt's CNN-based 3D Object Classification/Recognition framework. 
* The implementation is written in Python 2.7 (Anaconda distribution) in an Ubuntu 14.04 desktop with 128 GB RAM and an Nvidia GeForce GTX 1070 GPU. 
  (However, the code should run without any problems in a Windows environment as well). 
* The hierarchy of the code is as follows: 

  `classification3D_deep`:

   - config
   - data
   - networks
   - scripts
   - trained_nets
   - trained_svms
   
The core code is organized in several .py files located in folder `scripts`. 

## Configuration
Data, and training configuration files are stored in `config` folder in order to set the value of important parameters.
In addition, paths for locating data files, networks' architectures and trained models are also configured from file.
More specifically:

The data configuration file (`config/data_config.cfg`) contains the following parameters: 

- 'rows': 1st dimension of the 3D data
- 'cols': 2nd dimension of the 3D data
- 'depth': 3rd dimension of the 3D data
- 'classes': number of different semantic classes existing in the dataset
- 'n_rotations': number of rotations to be computed when data augmentation is used

Parameters related to the training stage can be adjusted from the train configuration file (`config/train_config.cfg`). These parameters are:

- 'learning rate': (default value=0.001)
- 'momentum': (default value=0.9)
- 'batch_size': (default value=32)
- 'epochs': number of epochs to train the network (default=100)
- 'loss': loss (or objective) function to use during training (default=categorical_crossentropy)

In the file `config/paths_config.cfg`, three important paths should be configured:

- 'networks_path': folder where the .py files with the networks' definitions exist
- 'trained_nets_path': folder where the networks trained with this software are stored

## Installation
To train and test the ConvNets, you will need:

- [Theano](http://deeplearning.net/software/theano/) 
- [Keras](https://keras.io/)
- [scikit-learn](http://scikit-learn.org/stable/) 
- [CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit) and 
  [cuDNN](https://developer.nvidia.com/cudnn) for GPU accelerations

A GPU-enabled installation of Theano and Keras is highly advised for faster network training. Details on how to exploit GPU can be found in their online documentation. 

## Data 
The employed neural networks are designed to accept as input voxelized versions of 3D meshes, i.e. 3D grids of size V x V x V containing the object of interest. In our experiments, V = 32. The initial format of 
the 3D models is assumed to be either .obj or .off. The conversion from obj/off format to volumetric representation is performed using Patrick Min's 'binvox' program available at: http://www.patrickmin.com/binvox/.

## Deep CNNs
Several architectures were designed and tested. Two networks are provided in this version (both architectures and pre-trained models in folders `networks` and `trained_nets` respectively) as examples. 

## Training a deep CNN
In order to train a deep Convolutional Neural Network, the `train_nn.py` script should be called either from a command window or a batch file providing 3 input arguments:

 1. Training data
 2. Deep network's architecture to be trained (.py file)
 3. A name to use for storing the trained network

e.g. python train_nn.py trainData.tar model1.py model1

**Note**: model1.py should contain a `get_model()` method receiving the parameters from the data configuration file as input arguments (i.e. rows, cols, depth, n_classes) 
and returning the definition of the functional model to be trained.

## Classifying using a deep CNN
In order to test a pre-trained deep Convolutional Neural Network and classify 3D models, the `classify.py` script should be called either from a command window or a batch file providing 2 input arguments:

 1. Test data (either a single 3D model in .npy.z, .npz or .obj format or multiple models stored as a .tar file - full path)
 2. A pretrained deep network (hdf5 file)

e.g. python classify.py testData.tar model1.h5 

At test time, 

- when a single 3D model is provided, class probabilities are returned along with the final classification assignment.
- when multiple 3D models are provided (as a .tar file), the test classification accuracy is computed and returned.  

In order to load and process an .obj file, the next two files should be downloaded and stored in folder `scripts`:

 1. the 'Binvox' executable provided by Patrick Min at: http://minecraft.gamepedia.com/Programs_and_editors/Binvox
 2. the script 'binvox_rw.py' provided by Daniel Maturana at: https://github.com/dimatura/binvox-rw-py
 
## Classifying using a SVM
Apart from training a deep neural network and use it end-to-end to perform 3D object classification, this software also provides the possibility to extract features from any layer of a trained deep neural network 
and perform the classification using a standard classifier, i.e. a Support Vector Machine (SVM). In this case, a SVM should be trained at first using the features extracted from a deep CNN for the training dataset and 
then, the trained SVM is used to predict the classification outcome at test time. The two scripts supporting this functionality are `train_svm.py` and `svm_classify.py`. For example, in order to train a SVM using the output 
of the 10th layer of the deep CNN 'model1.h5' the following command should be executed:

python train_svm.py trainData.tar model1.h5 10 trained_svm.pkl

The trained SVM will be stored in the file 'trained_svm.pkl'. In order to classify a 3D object using the trained SVM, the following command should be executed:

python svm_classify.py test_model.obj model1.h5 10 trained_svm.pkl

## Results 
For our experiments we used the [ModelNet10] dataset from the [3D ShapeNets](http://3dshapenets.cs.princeton.edu/) project.
Below are the classification accuracies we got on ModelNet10 test data.

|   Model   | Classification  Accuracy |
|:---------:|:------------------------:|
|  model_v5 |   89.13% (on 12x data)   |
| model_v12 |   90.53% (on 12x data)   |


## Acknowledgement
Data pre-processing was based on code provided by Daniel Maturana in his project [Voxnet](https://github.com/dimatura/voxnet).