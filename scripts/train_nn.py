from __future__ import absolute_import

import imp, sys
import numpy as np

import keras
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau

from sklearn.model_selection import train_test_split

import load_data
import dnn_io
import gen_utils

np.random.seed(1337)

paths_dict = gen_utils.load_paths()
data_dict = gen_utils.load_data_params()


def train_model(model, train_set, train_labels, lr, momentum, batch_size, epochs, loss, output_name):
    # Pre-processing
    train_set = load_data.preprocess_data(train_set)

    # Separate 20% of the training data for validation
    x_train_new, x_val_new, y_train_new, y_val_new = train_test_split(train_set, train_labels, test_size=0.2,
                                                                      random_state=4)
    # Compile the model...
    sgd_opt = keras.optimizers.SGD(lr=lr, momentum=momentum)
    model.compile(loss=loss, optimizer=sgd_opt, metrics=['accuracy'])

    # Callbacks for saving the model after each epoch + for reducing learning rate when val_loss has stopped improving
    callbacks_list = [ModelCheckpoint(paths_dict['trained_nets_path']+output_name + ".{epoch:03d}-{val_acc:.4f}.hdf5",
                    monitor='val_acc', verbose=1), ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=10)]

    # Train the model
    print("Training model...")
    hist = model.fit(x_train_new, y_train_new, validation_data=(x_val_new, y_val_new),
                     batch_size=batch_size, nb_epoch=epochs, shuffle=True, callbacks=callbacks_list)
    print("Training completed...")

    # Save the trained model (full)
    dnn_io.save_full_model(model, output_name)
    print("The trained network has been saved.")

    # Save model architecture and weights separately
    # dnn_io.save_model_parts(model, output_name)


def main(argv):
    usage = '\nUsage: python train_nn.py [train_data] [network_architecture] [output_net_name]\n'

    if len(argv) < 4:
        print usage
    else:
        # Input arguments
        data_file = argv[1]
        network = argv[2]
        output_name = argv[3]

        # Import the model's architecture
        network_design = imp.load_source(network, paths_dict['networks_path'] + network)

        # Read model's architecture from file
        model = network_design.get_model(data_dict['rows'], data_dict['cols'], data_dict['depth'],
                                         data_dict['n_classes'])

        # Load train parameters
        train_dict = gen_utils.load_train_params()

        # Load data
        dataset, final_labels = load_data.load_tar_data(data_file)

        # Train model and save
        train_model(model, dataset, final_labels, train_dict['learning_rate'], train_dict['momentum'],
                  train_dict['batch_size'], train_dict['epochs'], train_dict['loss'], output_name)

if __name__ == '__main__':
    main(sys.argv)
