import cStringIO as StringIO
import numpy as np
import tarfile, zlib
import sys, os
import platform
import subprocess

from keras.utils import np_utils

import binvox_rw
import process_data
import gen_utils
# sys.path.insert(0, '../utils')
# import gen_utils

data_dict = gen_utils.load_data_params()


# data augmentation by rotating and voxelizing each 3D model 'n_rotations' times.
def augment_data(data_file):
    # compute 12 rotations for test sample
    test_set = np.zeros((data_dict['n_rotations'], 1, data_dict['rows'], data_dict['cols'], data_dict['depth']))
    vertices_orig, faces_orig = read_objoff(data_file)
    for i in range(0, data_dict['n_rotations']):
        if '.off' in data_file:
            # save rotation to file as .off
            temp = data_file[0:data_file.find('.off')] + "_" + str(i + 1) + ".off"
            save_objoff(temp, process_data.compute_3d_rotation(vertices_orig,
                                                               i * (360.0 / data_dict['n_rotations'])), faces_orig)
        elif '.obj' in data_file:
            # save rotation to file as .obj
            temp = data_file[0:data_file.find('.obj')] + "_" + str(i + 1) + ".obj"
            save_objoff(temp, process_data.compute_3d_rotation(vertices_orig,
                                                               i * (360.0 / data_dict['n_rotations'])), faces_orig)

        # run binvox for rotation i
        test_set[i] = convert_obj2binvox(temp)

    return test_set


# read a 3D model's vertices and faces from an .obj or an .off (modelnet's style) file
def read_objoff(filename):
    with open(filename, 'r') as obj:
        data = obj.read()

    lines = data.splitlines()

    vertices = np.empty((0, 3), float)
    faces = []
    if lines[0] == 'OFF':
        elem = lines[1].split()
        num_vert = int(elem[0])
        num_faces = int(elem[1])

        del lines[0]
        del lines[0]

        idx = 0
        for line in lines:
            # split line based on white space
            elem = line.split()
            if len(elem) == 3 & idx < num_vert:
                v = np.array([float(elem[0]), float(elem[1]), float(elem[2])])
                v.shape = (1, 3)
                vertices = np.vstack((vertices, v))
                idx += 1
            else:
                # f = np.empty((0, 3), int)
                # f = np.append(f, elem[1])
                # f = np.append(f, elem[2])
                # f = np.append(f, elem[3])
                # faces = np.vstack([faces, f])

                faces.append(line)
    else:
        for line in lines:
            # split line based on white space
            elem = line.split()
            if elem:
                # read vertex
                if elem[0] == 'v':
                    v = np.array([float(elem[1]), float(elem[2]), float(elem[3])])
                    v.shape = (1, 3)
                    vertices = np.vstack((vertices,v))
                # read face
                elif elem[0] == 'f':
                    #f = np.empty((0, len(elem)-1), int)
                    #for i in range(1, len(elem)):
                        #f = np.append(f, elem[i].split('//')[0])
                    #faces = np.vstack([faces, line])

                    # save faces as strings (whole lines)
                    faces.append(line)
                else:
                    pass
    return vertices, faces


# save vertices and faces of a 3D model to an .obj or .off file
def save_objoff(filename, vertices, faces):
    with open(filename, "w") as f:

        if ".obj" in filename:
            f.write("# Obj File\n")
        elif ".off" in filename:
            f.write("OFF\n")
            f.write("".join([str(vertices.shape[0]), " ", str(faces.shape[0]), " 0\n"]))

        # write vertices to file
        for i in range(0, vertices.shape[0]):
            if ".obj" in filename:
                f.write("v ")
            for j in range(0, vertices.shape[1]):
                f.write(str(vertices[i][j]))
                f.write(" ")
            f.write("\n")
        # write faces to file
        for i in range(0, len(faces)):
            f.write(faces[i])
            f.write("\n")


# Load data (single .npy.z or .obj file or multiple .npy.z files stored in a .tar file)
def load_data_file(data_file):
    test_labels = []
    if '.tar' in data_file:
        test_set, test_labels = load_tar_data(data_file)
    elif '.npy.z' in data_file:
        test_set = load_npy_file(data_file)
    elif '.obj' in data_file:
        # test_set = convert_obj2binvox(data_file)

        test_set = augment_data(data_file)

    return test_set, test_labels


# For converting the .obj file to .binvox, we use the 'Binvox' program provided by Patrick Min:
# http://minecraft.gamepedia.com/Programs_and_editors/Binvox
# For loading the .binvox file into a numpy 3d array, we use the script 'binvox_rw.py' provided by Daniel Maturana:
# https://github.com/dimatura/binvox-rw-py
def convert_obj2binvox(data_file):
    if os.path.isfile(data_file):
        num = 1

        # save the model in the appropriate format for keras
        model3d = np.zeros((num, 1, data_dict['rows'], data_dict['cols'], data_dict['depth']))

        if 'Windows' in platform.platform():
            # Windows executable for converting .obj to .binvox
            command = "binvox.exe -d 32 -e " + data_file
            os.system(command)
        elif 'Linux' in platform.platform():
            # Run Windows executable for converting .obj to .binvox using Wine
            command = "wine binvox.exe -d 32 -e " + data_file
            # os.system(command)
            p = subprocess.Popen(command, bufsize=2048, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            p.wait()
            # read the result to a string
            result_str = p.stdout.read()

        # extract filename from full path
        if ".obj" in data_file:
            obj_name = data_file[data_file.rfind('/') + 1:data_file.find('.obj')]
        elif ".off" in data_file:
            obj_name = data_file[data_file.rfind('/') + 1:data_file.find('.off')]
        print obj_name

        # read .binvox file into a numpy 3D array
        with open(data_file[0:data_file.rfind('/')+1] + obj_name + ".binvox", 'rb') as f:
            array3d = binvox_rw.read_as_3d_array(f)

        model3d[0][0][:][:][:] = array3d.data.astype(int)

        return model3d
    else:
        print "IO Error! Check input data file... Full path is required."
        return None


# Load a single input data file (.npy.z)
def load_npy_file(data_file):
    if os.path.isfile(data_file):
        num = 1

        # save the train set in the appropriate format for keras
        model3d = np.zeros((num, 1, data_dict['rows'], data_dict['cols'], data_dict['depth']))

        # read the compressed data file
        compressed_file = open(data_file, 'r').read()
        buf = zlib.decompress(compressed_file)
        arr = np.load(StringIO.StringIO(buf))
        model3d[0][0][:][:][:] = arr

        return model3d
    else:
        print "IO Error! Check input data file... Full path is required."
        return None


# Load multiple input data files stored in a .tar file
def load_tar_data(data_file):
    # open .tar file
    try:
        tr_file = tarfile.open(data_file, mode="r:tar")
    except IOError as e:
        print "IO Error! Check input data file... Full path is required."
        return None, None

    # get the number of training samples
    samples = tr_file.getnames()
    num = len(samples)

    # save the train set in the appropriate format for keras
    dataset = np.zeros((num, 1, data_dict['rows'], data_dict['cols'], data_dict['depth']))
    labels = np.ones((num,),dtype = int)

    # read each compressed file until all train dataset is loaded
    for h in xrange(0, num-1):
        entry = samples[h]
        if entry is None:
            raise StopIteration()
        fileobj = tr_file.extractfile(entry)
        buf = zlib.decompress(fileobj.read())
        arr = np.load(StringIO.StringIO(buf))
        dataset[h][0][:][:][:] = arr

        # 10 classes: make indexes 0...9 for np_utils.to_categorical
        labels[h] = int(entry[entry.find('/')+1:entry.find('/')+4]) - 1

    # convert class vectors to binary class matrices
    final_labels = np_utils.to_categorical(labels, data_dict['n_classes'])

    # close the file stream
    tr_file.close()

    return dataset, final_labels


def main(argv):
    datafile = argv[1]

    # dataset, final_labels = load_tar_data(datafile)
    # print dataset.shape
    # print final_labels.shape

    read_objoff('dresser_0001.off')

if __name__ == '__main__':
    main(sys.argv)