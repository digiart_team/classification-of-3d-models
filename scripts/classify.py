import sys
import time
import numpy as np

import keras
from keras.utils import np_utils

import data_io
import dnn_io
import gen_utils
import process_data

data_dict = gen_utils.load_data_params()


def test_model(model, data, labels, batch_size):
    print("Evaluating model...")

    # one 3D model is provided, so the predicted label (i.e. class) is returned
    if len(labels) == 0:
        # Generate class probabilities for the input sample
        proba = model.predict(data)

        if data.shape[0] > 1:
            # Average probabilities over all rotations...
            final_proba = np.mean(proba, axis=0)
        else:
            final_proba = proba.T

        # Display class probabilities and final classification result
        print "\nPredicted class probabilities: "
        for i in range(0, len(final_proba)):
            print "%03d - %.05f" %(i+1, final_proba[i])

        # Get class prediction from the probabilities [#samples x 1]
        class_pred = np_utils.probas_to_classes(final_proba)
        print "\nThe 3D object is assigned to category: %03d" %(int(np.nonzero(class_pred)[0])+1)
    else:
        # whole test dataset provided in order to compute test classification accuracy

        # Evaluate the model with the test data
        score = model.evaluate(data, labels, batch_size=batch_size)
        print('Test score:', score[0])
        print('Test classification accuracy:', score[1])

        # Generate class probabilities for the input samples batch by batch.
        # proba = model.predict(data, batch_size=batch_size)
        # Get class predictions from the probabilities [#samples x 1]
        # classes = np_utils.probas_to_classes(proba)


def main(argv):
    start = time.clock()

    keras.initializations.my_init = dnn_io.my_init
    # or setattr(initializations, 'my_init', my_init)

    usage = '\nUsage: python classify.py [test_data] [pretrained_model]\n'

    if len(argv) < 3:
        print usage
    else:
        # Input arguments
        data_file = argv[1]
        pretrained_net = argv[2]

        # load test sample(s) (single .npy.z or .obj file or multiple .npy.z files stored in a .tar file)
        test_labels = []
        test_set, test_labels = data_io.load_data_file(data_file)

        if test_set is not None:
            # Pre-processing (as in train data)
            test_set = process_data.preprocess_data(test_set)

            # Load train parameters
            train_dict = gen_utils.load_train_params()

            # Load full model
            model = dnn_io.load_full_model(pretrained_net)
            # print model.summary()

            # Load architecture and weights separately
            # model = dnn_io.load_model_parts(pretrained_net, learning_rate, momentum, loss)

            # Compute network's performance on test data
            test_model(model, test_set, test_labels, train_dict['batch_size'])

    end = time.clock()
    print "Execution time in secs: %f" % (end-start)

if __name__ == '__main__':
    main(sys.argv)
