from keras.models import load_model, model_from_json, Model
from keras.optimizers import SGD
from keras import initializations

import gen_utils

paths_dict = gen_utils.load_paths()


# get features from a specific layer of a trained neural network
# layerIdx: Indices are based on order of horizontal graph traversal (bottom-up).
def get_features_from_dnn(input, pretrained_net, layer_idx):
    # create the original model
    model = load_full_model(pretrained_net)

    # get the desired layer
    intermediate_layer_model = Model(input=model.input, output=model.get_layer(index=layer_idx).output)
    # get the chosen layer's output
    intermediate_output = intermediate_layer_model.predict(input)

    return intermediate_output


# Load a single hdf5 file containing a network's architecture, weights, training params, optimizer state
def load_full_model(pretrained_net):
    model = load_model(paths_dict['trained_nets_path'] + pretrained_net)
    # print model.summary()
    # print model.get_weights()

    return model


# Load a networks's architecture and weights from two separate files (network from .json and weights from .h5)
def load_model_parts(pretrained_net, learning_rate, momentum, loss):
    # load json
    json_file = open(paths_dict['trained_nets_path'] + pretrained_net + ".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    # create model
    model = model_from_json(loaded_model_json)

    # compile model
    sgd_opt = SGD(lr=learning_rate, momentum=momentum)
    model.compile(loss=loss, optimizer=sgd_opt, metrics=['accuracy'])

    # load weights into new model
    model.load_weights(paths_dict['trained_nets_path'] + pretrained_net + "_weights.h5")

    return model


# Save full model: architecture, weights, training params, optimizer state
def save_full_model(model, output_name):
    model.save(paths_dict['trained_nets_path'] + output_name + "_full.h5")


# Save model's architecture and weights separately in a JSON and a HDF5 file respectively
def save_model_parts(model, output_name):
    # serialize model to JSON
    model_json = model.to_json()
    with open(paths_dict['trained_nets_path'] + output_name + ".json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to hdf5
    model.save_weights(paths_dict['trained_nets_path'] + output_name + "_weights.h5")


# for dense layers: initialization from a zero-mean Gaussian with sigma=0.01
def my_init(shape, name=None):
    return initializations.normal(shape, scale=0.01, name=name)
