import sys
import numpy as np

from sklearn.externals import joblib
from sklearn.metrics import accuracy_score

import keras
from keras.utils import np_utils

import dnn_io
import data_io

# bone classes
#class_id_to_name={
#    "1": "femora",
#    "2": "metacarpal_bone",
#    "3": "metatarsal_bone",
#    "4": "phalanx_of_finger",
#    "5": "phalanx_of_toe",
#    "6": "rib",
#    "7": "thoracic_vertebra"
#}


def classify(classifier, test_feats, test_labels):
    if len(test_labels) == 0:
        # Generate class probabilities for the input sample
        proba = classifier.predict_proba(test_feats)

        if test_feats.shape[0] > 1:
            # Average probabilities over all rotations...
            final_proba = np.mean(proba, axis=0)
        else:
            final_proba = proba.T

        print "\nPredicted class probabilities: "
        for i in range(0, len(final_proba)):
            print "%03d - %.05f" % (i+1, final_proba[i])

        print "\nThe 3D object is assigned to category: %03d" % (int(np.argmax(final_proba)+1))
    else:
        # get prediction probabilities
        proba = classifier.predict_proba(test_feats)
        # Get class predictions from the probabilities [#samples x 1]
        classes = np_utils.probas_to_classes(proba)

        test_lbls = np.empty(len(test_labels))
        for i in range(0, len(test_labels), 1):
            test_lbls[i] = int(test_labels[i].tolist().index(1))

        # compute accuracy
        acc = accuracy_score(test_lbls, classes)
        print("\nAccuracy: %s" % acc)


def main(argv):
    keras.initializations.my_init = dnn_io.my_init
    # or setattr(initializations, 'my_init', my_init)

    usage = '\nUsage: python svm_classify.py [test_data] [pretrained_net] [layer_index] [svm_name]\n'

    if len(argv) < 5:
        print usage
    else:
        # Input arguments
        data_file = argv[1]
        pretrained_net = argv[2]
        layer_idx = int(argv[3])
        svm_name = argv[4]

    # load test sample(s)
    test_set, test_labels = data_io.load_data_file(data_file)

    # get features from pretrained dnn
    dnn_output = dnn_io.get_features_from_dnn(test_set, pretrained_net, layer_idx)

    # load pretrained SVM
    classifier = joblib.load(svm_name)

    # classify using SVM
    classify(classifier, dnn_output, test_labels)


if __name__ == '__main__':
    main(sys.argv)