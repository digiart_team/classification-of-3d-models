import ConfigParser


# read a configuration file
def parse_config(filename):
    config = ConfigParser.ConfigParser()
    config.read(filename)
    return config


# load the data-related parameters from configuration file
def load_data_params():
    config = parse_config('../config/data_config.cfg')

    data_dict = {'rows': config.getint('default', 'rows'),
                 'cols': config.getint('default', 'cols'),
                 'depth': config.getint('default', 'depth'),
                 'n_classes': config.getint('default', 'n_classes'),
                 'n_rotations': config.getint('default', 'n_rotations')}

    return data_dict


# load the training parameters from configuration file
def load_train_params():
    train_config = parse_config('../config/train_config.cfg')

    train_dict = {'learning_rate': train_config.getfloat('default', 'learning_rate'),
                  'momentum': train_config.getfloat('default', 'momentum'),
                  'batch_size': train_config.getint('default', 'batch_size'),
                  'epochs': train_config.getint('default', 'epochs'),
                  'loss': train_config.get('default', 'loss')}

    return train_dict


# load the paths leading to data, networks' architectures and trained nets from configuration file
def load_paths():
    paths_config = parse_config('../config/paths_config.cfg')

    paths_dict = {'networks_path': paths_config.get('default', 'networks_path'),
                  'trained_nets_path': paths_config.get('default', 'trained_nets_path')}

    return paths_dict