import sys
import numpy as np

from sklearn.externals import joblib
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score, cross_val_predict
import sklearn.metrics as metrics

import keras

import dnn_io
import data_io


# function for training a SVM model using features from a dnn
def svm_train(dnn_output, labels, svm_name):
    # split dataset to train and test samples (70% - 30% respectively)
    train_feats, test_feats, train_lbls_temp, test_lbls_temp = train_test_split(dnn_output, labels, test_size=0.3, random_state=0)

    # convert train/test labels to numerical form (1..num_classes)
    train_lbls = np.empty(train_lbls_temp.shape[0])
    for i in range(0, train_lbls_temp.shape[0]):
        train_lbls[i] = np.nonzero(train_lbls_temp[i])[0] + 1

    test_lbls = np.empty(test_lbls_temp.shape[0])
    for i in range(0, test_lbls_temp.shape[0]):
        test_lbls[i] = np.nonzero(test_lbls_temp[i])[0] + 1

    # create/train a linear SVM and check score
    clf = svm.SVC(kernel='linear', C=1, gamma=1, probability=True)
    clf.fit(train_feats, train_lbls)
    clf.score(train_feats, train_lbls)

    # predict output for test set
    predicted = clf.predict(test_feats)
    metrics.accuracy_score(test_lbls, predicted)

    # Save trained SVM to file
    joblib.dump(clf, svm_name)


def main(argv):
    keras.initializations.my_init = dnn_io.my_init
    # or setattr(initializations, 'my_init', my_init)

    usage = '\nUsage: python train_svm.py [data] [pretrained_net] [layer_index] [svm_name] \n'

    if len(argv) < 5:
        print usage
    else:
        # Input argument
        data_file = argv[1]
        pretrained_net = argv[2]
        layer_idx = int(argv[3])
        svm_name = argv[4]

    # load full dataset
    dataset, labels = data_io.load_data_file(data_file)

    # get features for the dataset from the trained neural network
    dnn_output = dnn_io.get_features_from_dnn(dataset, pretrained_net, layer_idx)

    # train svm with provided data and save to file
    svm_train(dnn_output, labels, svm_name)

if __name__ == '__main__':
    main(sys.argv)