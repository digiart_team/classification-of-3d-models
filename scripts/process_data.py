import numpy as np
import math


# data augmentation by generating rotations of each 3d model
# (rotation is performed before voxelization)
# theta = (viewpoint-1)*angle
def compute_3d_rotation(vertices, theta):
    vertex_max = np.amax(vertices, axis=0)
    vertex_min = np.amin(vertices, axis=0)

    center = (vertex_max + vertex_min) / 2.0

    vertices -= center

    theta = theta * math.pi / 180.0

    rot = np.array([[math.cos(theta), -math.sin(theta), 0], [math.sin(theta), math.cos(theta), 0], [0, 0, 1]])

    vertices = np.dot(vertices, rot)

    return vertices


# Pre-processing
def preprocess_data(data):
    data = data.astype('float32')
    # data -= np.mean(data)
    # data /=np.max(data)
    data = 2.0 * data - 1.0

    return data
