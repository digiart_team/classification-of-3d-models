from __future__ import absolute_import
import imp

import keras
from keras.models import Model
from keras.layers import Input, Dense, merge
from keras.layers import Convolution3D, MaxPooling3D
from keras.layers import Dropout, Activation, Flatten
from keras.layers import BatchNormalization
from keras.regularizers import l2

dnn_utils = imp.load_source('dnn_utils', '../scripts/dnn_io.py')
keras.initializations.my_init = dnn_utils.my_init


def get_model(rows,cols,depth,n_classes):
    inputs = Input(shape=(1,rows,cols,depth))
    # l_conv1
    a=Convolution3D(32, 3, 3, 3, init='he_normal', activation='relu', input_shape=(1, rows, cols, depth), border_mode='same', subsample=(1, 1, 1), dim_ordering='default', bias=True)(inputs)
    a=BatchNormalization(input_shape=(1, rows, cols, depth), axis=1)(a)
    a=Activation('relu')(a)
    # identity_block
    x=Convolution3D(32, 3, 3, 3, init='he_normal', activation='relu', border_mode='same', subsample=(1, 1, 1), dim_ordering='default', bias=True)(a)
    x=BatchNormalization(axis=1)(x)
    x=Activation('relu')(x)
    x=Convolution3D(32, 3, 3, 3, init='he_normal', activation='relu', border_mode='same', subsample=(1, 1, 1), dim_ordering='default', bias=True)(x)
    x=BatchNormalization(axis=1)(x)
    y = merge([a, x], mode='sum')
    y=Activation('relu')(y)
    # conv_block_a
    b=Convolution3D(64, 2, 2, 2, init='he_normal', activation='relu', border_mode='valid', subsample=(2, 2, 2), dim_ordering='default', bias=True)(y)
    b=BatchNormalization(axis=1)(b)
    b=Activation('relu')(b)
    b=Convolution3D(64, 3, 3, 3, init='he_normal', activation='relu', border_mode='same', subsample=(1, 1, 1), dim_ordering='default', bias=True)(b)
    b=BatchNormalization(axis=1)(b)
    # conv_block_b
    c=Convolution3D(64, 1, 1, 1, init='he_normal', activation='relu', border_mode='valid', subsample=(2, 2, 2), dim_ordering='default', bias=True)(y)
    c=BatchNormalization(axis=1)(c)
    z = merge([b, c], mode='sum')
    z=Activation('relu')(z)
    # l_pool
    z=MaxPooling3D(pool_size=(2,2,2))(z)
    # l_drop
    z=Dropout(float(0.5))(z) # !!!
    z=Flatten()(z)
    # l_fc1
    z=Dense(128, init=dnn_utils.my_init)(z)
    # l_fc2
    z=Dense(n_classes, init=dnn_utils.my_init, activation='relu', W_regularizer=l2(0.001))(z)
    predictions = Activation('softmax')(z)

    # create the model
    model = Model(input=inputs, output=predictions)

    return model


