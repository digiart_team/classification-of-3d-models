from __future__ import absolute_import
import imp

import keras
from keras.models import Model
from keras.layers import Input, Dense
from keras.layers import Convolution3D, MaxPooling3D
from keras.layers import Dropout, Activation, Flatten

dnn_utils = imp.load_source('dnn_utils', '../scripts/dnn_io.py')
keras.initializations.my_init = dnn_utils.my_init


def get_model(rows, cols, depth, n_classes):
    inputs = Input(shape=(1, rows, cols, depth))

    # l_conv1_1
    a = Convolution3D(32, 3, 3, 3, init='he_normal', activation='relu', input_shape=(1, rows, cols, depth),
                  border_mode='valid', subsample=(1, 1, 1), dim_ordering='default', bias=True)(inputs)
    # l_conv1_2
    a = Convolution3D(32, 3, 3, 3, init='he_normal', activation='relu', border_mode='valid', subsample=(1, 1, 1),
                  dim_ordering='default', bias=True)(a)
    a = MaxPooling3D(pool_size=(2, 2, 2))(a)
    a = Dropout(0.2)(a)
    # l_conv2_1
    a = Convolution3D(64, 3, 3, 3, init='he_normal', activation='relu', border_mode='valid', subsample=(1, 1, 1),
                  dim_ordering='default', bias=True)(a)
    # l_conv2_2
    a = Convolution3D(64, 3, 3, 3, init='he_normal', activation='relu', border_mode='valid', subsample=(1, 1, 1),
                  dim_ordering='default', bias=True)(a)
    # l_conv2_3
    a = Convolution3D(64, 3, 3, 3, init='he_normal', activation='relu', border_mode='valid', subsample=(1, 1, 1),
                  dim_ordering='default', bias=True)(a)
    a = MaxPooling3D(pool_size=(2, 2, 2))(a)
    a = Dropout(0.3)(a)
    a = Flatten()(a)
    # l_fc1
    a = Dense(128, init=dnn_utils.my_init)(a)
    a = Dropout(0.4)(a)
    # l_fc2
    a = Dense(n_classes, init=dnn_utils.my_init, activation='relu')(a)
    predictions = Activation('softmax')(a)

    # create the model
    model = Model(input=inputs, output=predictions)

    return model
